#include <avr/io.h>
#include <avr/eeprom.h>
#include "drivers/board.h"
#include "drivers/adc.h"
#include "drivers/motor.h"
#include "drivers/com.h"
#include "drivers/gyro.h"
#include "drivers/sanchez_conf.h"
int main(void)
{
    clock_init();
    board_init();
	adc_init();
    radio_init();
    motor_init();
    servo_init();
    gyro_init();
    gyro_zero_gyro();

	uint32_t redetection_timer = millis();
    int8_t error = 0;
    uint16_t motor_current_counter=0;
	
	if(RST.STATUS & 1)
	{
		RST.STATUS = 1;
		CCPWrite(&RST.CTRL,RST_SWRST_bm);
	}
    while(1)
    {
		uint32_t while_start_time = millis();
		driving_input();
		while(eeprom_read_byte(0))
		{
			rgb_set(WHITE);
			standing_input();
		}
		uint16_t * reading = get_readings();
		int32_t current_gyro = get_gyro();
		corrected_speed = CLAMP((fwd_speed*(60 - reading[VOLTAGE_PIN]/5)/235)*5,150,350); //voltage * pwm = const
		error = reading[RIGHT] - reading[LEFT];
		steering(CLAMP((driving_p * error / driving_pd),STEERING_MAX_LEFT,STEERING_MAX_RIGHT),steering_offset);
		motor_speed(corrected_speed);
		
		if((reading[MOTOR_CURRENT_PIN]>motor_current_threshold) && (millis()-redetection_timer > REDETECTION_TIMEOUT)) 
			motor_current_counter ++;
		else 
			motor_current_counter =0;
		
		int8_t wall_right = reading[RIGHT] < RIGHT_CLOSE_DIST && reading[MIDDLE] < MIDDLE_CLOSE_DIST;
		int8_t wall_left = reading[LEFT] < LEFT_CLOSE_DIST && reading[MIDDLE] < MIDDLE_CLOSE_DIST;
		int8_t motor_current_overloaded = motor_current_counter > motor_current_counter_threshold;
		int8_t robot_bumped = ABS(current_gyro)> gyro_bump_threshold;
		
        if (wall_right && millis()-redetection_timer > REDETECTION_TIMEOUT)
        {
            rgb_set(BLUE);
            uint32_t timeout = millis();
            int32_t old_angle = get_angle();
			steering(STEERING_MAX_RIGHT,steering_offset);
			motor_speed(reverse_speed);
            while ((reading[MIDDLE] < MIDDLE_CLOSE_DIST + REVERSE_DIST_DELTA || reading[RIGHT] < RIGHT_CLOSE_DIST+REVERSE_DIST_DELTA)  && ((millis() - timeout) < 1000) && (ABS((old_angle-get_angle())<2000)))
				reading = get_readings();
			redetection_timer = millis();
			motor_current_overloaded = 0;
        }
        else if (wall_left && millis()-redetection_timer > REDETECTION_TIMEOUT)
        {
            rgb_set(YELLOW);
            uint32_t timeout = millis();
            int32_t old_angle = get_angle();
            steering(STEERING_MAX_LEFT,steering_offset);
            motor_speed(reverse_speed);
            while ((reading[MIDDLE] < MIDDLE_CLOSE_DIST + REVERSE_DIST_DELTA || reading[LEFT] < RIGHT_CLOSE_DIST+REVERSE_DIST_DELTA) && ((millis() - timeout) < 1000) && (ABS((old_angle-get_angle())<2000)))
				reading = get_readings();
			redetection_timer = millis();
			motor_current_overloaded = 0;
        }
        else if(motor_current_overloaded)
        {
            rgb_set(MAGENTA);
            uint32_t timeout = millis();
            int32_t old_angle = get_angle();
			motor_speed(reverse_speed);
			if (reading[LEFT] > reading[RIGHT])
			{
				steering(STEERING_MAX_RIGHT,steering_offset);
			}
			else
			{
				steering(STEERING_MAX_LEFT,steering_offset);
			}
            while (ABS((old_angle-get_angle()) < 1000 && millis()-timeout < 750));
            motor_current_counter = 0;
			redetection_timer = millis();
        }
		else if (robot_bumped)
		{	
			uint32_t timeout = millis();
			int32_t target_angle = get_last_good_angle();
			
			while((get_angle()<target_angle-300 || get_angle()>target_angle+300) && (millis()-timeout) < 5000)
			{
				rgb_set(RED);
				if(reading[MOTOR_CURRENT_PIN]>motor_current_threshold) 
					motor_current_counter ++;
				else 
					motor_current_counter =0;
				motor_current_overloaded = (motor_current_counter>motor_current_counter_threshold/8);
				backup_routine(target_angle,motor_current_overloaded);
				if(motor_current_overloaded) 
					motor_current_counter = 0;
			}	
		}
		else
			rgb_set(GREEN);
			
		if(debug_mode)
		{
			if(millis()%50 < 3)
			{
				 	char buff[50];
				 	sprintf(buff, ",%4d,%4d,%4d,%4d,%4d,%4d,%4ld,%4ld,%4ld,\n\r",reading[LEFT], reading[MIDDLE], reading[RIGHT], reading[MOTOR_CURRENT_PIN], corrected_speed,reading[VOLTAGE_PIN]*30+5800,get_gyro(),get_angle(),millis());
					radio_puts(buff);
			}
		}		
		while(millis()-while_start_time < 3);
    }
	
}

void backup_routine(int32_t target_angle, int8_t motor_current_overloaded)
{

	uint16_t * dist = get_readings();

	int32_t delta_angle = target_angle - get_angle();
	if((dist[LEFT]<20)||dist[MIDDLE]<20||dist[RIGHT]<20)
	{
		if(motor_current_overloaded)
		{
			uint32_t timeout = millis();
			motor_speed(corrected_speed);
			steering(STEERING_STRAIGHT,steering_offset);
			rgb_set(MAGENTA);
			while(millis()-timeout < 500);
		}
		motor_speed(reverse_speed);
		if (ABS(delta_angle) > GYRO_CIRCLE_CAP/2)
		{
			steering(get_sign(delta_angle) * STEERING_MAX_RIGHT,steering_offset);
		}
		else
		{
			steering(get_sign(delta_angle) * (STEERING_MAX_LEFT), steering_offset);
		}
		
	}
	else if((dist[LEFT]>25)&&dist[MIDDLE]>25&&dist[RIGHT]>25)
	{
		if(motor_current_overloaded)
		{
			uint32_t timeout = millis();
			motor_speed(reverse_speed);
			steering(STEERING_STRAIGHT,steering_offset);
			rgb_set(MAGENTA);
			while(millis()-timeout < 500);
		}
		motor_speed(corrected_speed-50);
		if (ABS(delta_angle) < GYRO_CIRCLE_CAP/2)
		{
			steering(get_sign(delta_angle) * STEERING_MAX_RIGHT,steering_offset);
		}
		else
		{
			steering(get_sign(delta_angle) * STEERING_MAX_LEFT,steering_offset);
		}
	}
	_delay_ms(25);
}
int8_t get_sign(int32_t input)
{
	if (input > 0) return 1;
	if (input < 0) return -1;
	return 0;
}
void driving_input()
{
	char radio_buffer=radio_getc_nolock();
	if(radio_buffer == 'h' || sw2_read())
	{
		eeprom_update_byte(0,1);
		steering(STEERING_STRAIGHT,steering_offset);
		motor_speed(MOTOR_STANDSTILL);
		char buff[20];
		sprintf(buff, "Stopped\n\r");
		radio_puts(buff);
		while(sw2_read());
		_delay_ms(50);
	}
	else if(radio_buffer == 'a')
	{
		steering_offset += 3;
 		char buff[20];
		sprintf(buff, "Steering %4d \n\r",715-steering_offset);
		radio_puts(buff);
	}
	else if(radio_buffer == 'q')
	{
		if(debug_mode) debug_mode = 0;
		else debug_mode = 1;
		char buff[20];
		sprintf(buff, "Debug mode %4d \n\r",debug_mode);
		radio_puts(buff);
	}
	else if (radio_buffer == 'd')
	{
		steering_offset -= 3;
		char buff[20];
		sprintf(buff, "Steering %4d \n\r",715-steering_offset);
		radio_puts(buff);
	}
	else if (radio_buffer == 'w')
	{
		fwd_speed += 3;
		char buff[20];
		sprintf(buff, "Speed %4d \n\r",fwd_speed);
		radio_puts(buff);
	}
	else if (radio_buffer == 's')
	{
		fwd_speed -= 3;
		char buff[20];
		sprintf(buff, "Speed %4d \n\r",fwd_speed);
		radio_puts(buff);
	}
	else if (radio_buffer == 'p')
	{
		driving_p ++;
		char buff[20];
		sprintf(buff, "Driving_P %4d \n\r",driving_p);
		radio_puts(buff);
	}
	else if (radio_buffer == 'o')
	{
		driving_p --;
		char buff[20];
		sprintf(buff, "Driving_P %4d \n\r",driving_p);
		radio_puts(buff);
	}
	else if (radio_buffer == 'k')
	{
		gyro_bump_threshold -= 100;
		char buff[20];
		sprintf(buff, "Gyro bump threshold %4d \n\r",gyro_bump_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'l')
	{
		gyro_bump_threshold += 100;
		char buff[20];
		sprintf(buff, "Gyro bump threshold %4d \n\r",gyro_bump_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'i') 
	{
		char buff[100];
		sprintf(buff, "h - stop\n\rg - go\n\rw - more speed %4d\n\rs - less speed\n\rp - more pid %4d\n\r",fwd_speed,driving_p);
		radio_puts(buff);
		sprintf(buff, "o - less pid %4d\n\ra - steer offset left %4d\n\rd - steer offset right\n\r",driving_p,715-steering_offset);
		radio_puts(buff);
		sprintf(buff, "m - more current %4d\n\rn - less current\n\rM - more current time %4d\n\rN - less current time\n\r",motor_current_threshold,motor_current_counter_threshold);
		radio_puts(buff);
		sprintf(buff, "gyro zero: %4ld seconds online %4lu\n\r",get_gyro_zero(), millis()/1000);
		radio_puts(buff);
	}
	
}
void standing_input()
{
	char radio_buffer=radio_getc_nolock();
	if(sw2_read() || radio_buffer == 'g')
	{
		eeprom_update_byte(0,0);
		char buff[20];
		sprintf(buff, "Started\n\r");
		radio_puts(buff);
		while(sw2_read());
		_delay_ms(50);
	}
	else if(radio_buffer == 'q')
	{
		if(debug_mode) debug_mode = 0;
		else debug_mode = 1;
		char buff[20];
		sprintf(buff, "Debug mode %4d \n\r",debug_mode);
		radio_puts(buff);
	}
	else if(radio_buffer == 'a')
	{
		steering_offset += 3;
		char buff[20];
		sprintf(buff, "Steering %4d \n\r",715-steering_offset);
		radio_puts(buff);
	}
	else if (radio_buffer == 'k')
	{
		gyro_bump_threshold -= 100;
		char buff[20];
		sprintf(buff, "Gyro bump threshold %4d \n\r",gyro_bump_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'l')
	{
		gyro_bump_threshold += 100;
		char buff[20];
		sprintf(buff, "Gyro bump threshold %4d \n\r",gyro_bump_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'd')
	{
		steering_offset -= 3;
		char buff[20];
		sprintf(buff, "Steering %4d \n\r",715-steering_offset);
		radio_puts(buff);
	}
	else if (radio_buffer == 'w')
	{
		fwd_speed += 3;
		char buff[20];
		sprintf(buff, "Speed %4d \n\r",fwd_speed);
		radio_puts(buff);
	}
	else if (radio_buffer == 's')
	{
		fwd_speed -= 3;
		char buff[20];
		sprintf(buff, "Speed %4d \n\r",fwd_speed);
		radio_puts(buff);
	}
	else if (radio_buffer == 'p')
	{
		driving_p ++;
		char buff[20];
		sprintf(buff, "Driving_P %4d \n\r",driving_p);
		radio_puts(buff);
	}
	else if (radio_buffer == 'o')
	{
		driving_p --;
		char buff[20];
		sprintf(buff, "Driving_P %4d \n\r",driving_p);
		radio_puts(buff);
	}
	else if (radio_buffer == 'm')
	{
		motor_current_threshold +=25;
		char buff[20];
		sprintf(buff, "Current threshold %4d \n\r",motor_current_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'n')
	{
		motor_current_threshold -=25;
		char buff[20];
		sprintf(buff, "Current threshold %4d \n\r",motor_current_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'M')
	{
		motor_current_counter_threshold +=25;
		char buff[20];
		sprintf(buff, "Driving_P %4d \n\r",motor_current_counter_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'N')
	{
		motor_current_counter_threshold -=25;
		char buff[20];
		sprintf(buff, "Driving_P %4d \n\r",motor_current_counter_threshold);
		radio_puts(buff);
	}
	else if (radio_buffer == 'R')
	{
		motor_speed(reverse_speed);
	}
	else if (radio_buffer == 'S')
	{
		motor_speed(MOTOR_STANDSTILL);
		steering(STEERING_STRAIGHT,steering_offset);
	}
	else if (radio_buffer == 'A')
	{
		steering(STEERING_MAX_LEFT,steering_offset);
	}
	else if (radio_buffer == 'D')
	{
		steering(STEERING_MAX_RIGHT,steering_offset);
	}
	else if (radio_buffer == 'W')
	{
		motor_speed(fwd_speed);
	}
	else if (radio_buffer == 'i')
	{
		char buff[100];
		sprintf(buff, "h - stop\n\rg - go\n\rw - more speed %4d\n\rs - less speed\n\rp - more pid %4d\n\r",fwd_speed,driving_p);
		radio_puts(buff);
		sprintf(buff, "o - less pid %4d\n\ra - steer offset left %4d\n\rd - steer offset right\n\r",driving_p,715-steering_offset);
		radio_puts(buff);
		sprintf(buff, "m - more current %4d\n\rn - less current\n\rM - more current time %4d\n\rN - less current time\n\r",motor_current_threshold,motor_current_counter_threshold);
		radio_puts(buff);
		sprintf(buff, "gyro zero: %4ld seconds online %4lu\n\r",get_gyro_zero(), millis()/1000);
		radio_puts(buff);
	}
	else if (radio_buffer == 'I')
	{
		uint16_t * reading = get_readings();
		char buff[50]; 
		sprintf(buff, "%4d,%4d,%4d,%4d,%4d,%4ld\n\r",reading[LEFT], reading[MIDDLE], reading[RIGHT], reading[MOTOR_CURRENT_PIN], reading[VOLTAGE_PIN]*30+5800, get_angle()); //voltage adc to mV
		radio_puts(buff);
	}
	
}