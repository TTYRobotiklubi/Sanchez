#ifndef SANCHEZ_H
#define SANCHEZ_H

#include <avr/io.h>

#define RIGHT 0
#define MIDDLE 1
#define LEFT 4
#define MOTOR_CURRENT_PIN 3
#define VOLTAGE_PIN 2



#define REDETECTION_TIMEOUT 1000
#define MIDDLE_CLOSE_DIST 20
#define RIGHT_CLOSE_DIST 11
#define LEFT_CLOSE_DIST 11
#define REVERSE_DIST_DELTA 3



#define GYRO_TURNING_THRESHOLD 700

#define STEERING_MAX_RIGHT 100
#define STEERING_MAX_LEFT -100
#define STEERING_STRAIGHT 0
#define MOTOR_STANDSTILL 0

#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))
#define ABS(a)	   (((a) < 0) ? -(a) : (a))

int8_t get_sign(int32_t);
void backup_routine(int32_t,int8_t);
void driving_input();
void standing_input();
int16_t motor_current_threshold = 650;
uint16_t motor_current_counter_threshold = 250;
int16_t fwd_speed = 220;
int16_t corrected_speed = 235;
int16_t reverse_speed = -200;
int16_t steering_offset = 715;
int8_t driving_p  = 20;
int8_t driving_pd = 10;
int8_t debug_mode = 0;
uint16_t gyro_bump_threshold = 3500;

#endif /* SANCHEZ_H */
