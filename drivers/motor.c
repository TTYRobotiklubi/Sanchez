#include "motor.h"

void servo_init(void)
{
    PORT_SetPinsAsOutput(&PORTC,1);
    PORT_ClearPins(&PORTC,1);
    TC0_ConfigWGM(&TCC0, TC_WGMODE_SS_gc);
    TC_SetPeriod(&TCC0, 9999);
    TC0_ConfigClockSource(&TCC0, TC_CLKSEL_DIV64_gc);

    TC_SetCompareA(&TCC0, 750);
    TC0_EnableCCChannels(&TCC0, TC0_CCAEN_bm);
}
void motor_init(void)
{

	PORT_SetPinsAsOutput(&PORTC, (7 << 5));
	PORT_ClearPins(&PORTC, (7 << 5));

    TC_SetPeriod(&TCC1,1000);
    TC1_ConfigWGM(&TCC1,TC_WGMODE_SS_gc);
    TC1_DisableCCChannels(&TCC1,(TC1_CCAEN_bm|TC1_CCBEN_bm));
    TC1_ConfigClockSource(&TCC1,TC_CLKSEL_DIV1_gc);
    TC1_EnableCCChannels(&TCC1, (TC1_CCAEN_bm|TC1_CCBEN_bm));

    TC_SetCompareA (&TCC1,0);
    TC_SetCompareB (&TCC1,0);
}

void steering(int8_t position, int16_t offset)
{
	TC_SetCompareA(&TCC0, offset-((int16_t)position*5/3));
}
int16_t motor_speed(int16_t position)
{
	if (position < 0)
	{
		PORTC_OUTSET = BIN;
		PORTC_OUTCLR = AIN;
		TC_SetCompareB (&TCC1,-1*position);
		TC_SetCompareA (&TCC1,-1*position);
	}
	else if (position > 0)
	{
		PORTC_OUTCLR = BIN;
		PORTC_OUTSET = AIN;
		TC_SetCompareB (&TCC1,position);
		TC_SetCompareA (&TCC1,position);
	}
	else 
	{
		PORTC_OUTCLR = BIN;
		PORTC_OUTCLR = AIN;
	}
	return position;
}
