#ifndef BOARD_H_
#define BOARD_H_

#ifndef F_CPU
#define F_CPU 32000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include "drivers/port_driver.h"
#include "drivers/clksys_driver.h"
#include "drivers/tc_driver.h"

#define SW_PORT		PORTD
#define SW1			(1<<4)
#define SW2			(1<<5)
#define SW_BLACK	SW1
#define SW_RED		SW1

#define LED_PORT	PORTB
#define LED_GREEN	2
#define LED_BLUE	3
#define LED_RED		1

typedef enum RGB_colour_enum
{
    OFF		= 0,
    RED		= 1,
    GREEN	= 2,
    YELLOW	= 3,
    BLUE	= 4,
    MAGENTA	= 5,
    PINK	= 5,
    TEAL	= 6,
    WHITE	= 7,
} RGB_colour;

#define MOTOR_PORT	PORTD
#define AIN1		3
#define BIN1		0
#define AIN2		2
#define BIN2		1
#define MOTOR_PINMASK	0x0F

#define COMPORT		PORTE
#define TXD			3
#define RXD			2

#define SDA			0
#define SCL			1

#define SYSTICK_TIMER			TCE0
#define SYSTICK_TIMER_INTERRUPT	TCE0_OVF_vect

extern volatile uint32_t systick;

inline void board_init()
{
    PORT_SetPinsAsOutput(&PORTB,0x0E);
    PORT_SetPinsAsInput(&PORTD,0x30);

    TC_SetPeriod(&SYSTICK_TIMER,32000);
    TC0_ConfigWGM(&SYSTICK_TIMER,TC_WGMODE_NORMAL_gc);
    TC0_SetOverflowIntLevel(&SYSTICK_TIMER,TC_OVFINTLVL_MED_gc);
    TC0_ConfigClockSource(&SYSTICK_TIMER,TC_CLKSEL_DIV1_gc);
	
    PMIC.CTRL |= PMIC_LOLVLEX_bm|PMIC_MEDLVLEX_bm|PMIC_HILVLEX_bm;
    sei();
}

inline void rgb_set(RGB_colour colour)
{
    PORT_SetOutputValue(&LED_PORT,((colour & 7) << 1)|(1 & PORTB.OUT));
}

inline int sw1_read()
{
    return (((~PORT_GetPortValue(&SW_PORT) & SW1) >> 4));
}

inline int sw2_read()
{
    return (((~PORT_GetPortValue(&SW_PORT) & SW2) >> 5));
}

inline uint32_t millis()
{
    uint32_t temp = 0;
    cli();
    temp = systick;
    sei();
    return temp;
}

inline void clock_init()
{
    CLKSYS_Enable( OSC_RC32MEN_bm );
    CLKSYS_Prescalers_Config( CLK_PSADIV_1_gc, CLK_PSBCDIV_1_1_gc );
    do {}
    while ( CLKSYS_IsReady( OSC_RC32MRDY_bm ) == 0 );
    CLKSYS_Main_ClockSource_Select( CLK_SCLKSEL_RC32M_gc );
}

#endif /* BOARD_H_ */