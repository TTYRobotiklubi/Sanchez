#include "adc.h"
#include <avr/eeprom.h>

uint16_t distance_lookup[150] =
{
	2040,
	2040,
	2040,
	2040,
	2040,
	2040,
	2040,
	1830,
	1655,
	1480,
	1394,
	1308,
	1222,
	1136,
	1050,
	1012,
	974,
	936,
	898,
	860,
	838,
	815,
	793,
	770,
	748,
	725,
	703,
	680,
	658,
	635,
	624,
	613,
	602,
	591,
	580,
	569,
	558,
	547,
	536,
	525,
	518,
	510,
	503,
	495,
	488,
	480,
	473,
	465,
	458,
	450,
	446,
	441,
	437,
	432,
	428,
	423,
	419,
	414,
	410,
	405,
	402,
	398,
	395,
	391,
	388,
	384,
	381,
	377,
	374,
	370,
	368,
	366,
	363,
	361,
	359,
	357,
	354,
	352,
	350,
	348,
	345,
	343,
	341,
	339,
	336,
	334,
	332,
	330,
	327,
	325,
	324,
	323,
	321,
	320,
	319,
	318,
	316,
	315,
	314,
	313,
	311,
	310,
	309,
	308,
	306,
	305,
	304,
	303,
	301,
	300,
	299,
	298,
	296,
	295,
	294,
	293,
	291,
	290,
	289,
	288,
	286,
	285,
	284,
	283,
	281,
	280,
	279,
	278,
	276,
	275,
	275,
	274,
	274,
	273,
	273,
	272,
	272,
	271,
	271,
	270,
	270,
	269,
	269,
	268,
	268,
	267,
	267,
	266,
	266,
	265,


};
uint16_t get_dist(uint16_t input)
{
	for (int i=5; i<120; i++)
	if (distance_lookup[i] < input)
	return i;
	return 120;
}
void adc_init(void)
{
    ADC_CalibrationValues_Load(&ADCA);

    ADC_ConvMode_and_Resolution_Config(&ADCA, true, ADC_RESOLUTION_12BIT_gc);

    ADC_Reference_Config(&ADCA, ADC_REFSEL_INTVCC_gc);

    ADC_Prescaler_Config(&ADCA, ADC_PRESCALER_DIV64_gc);
	
    ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH0,
                                     ADC_CH_INPUTMODE_SINGLEENDED_gc,
                                     ADC_CH_GAIN_1X_gc);
    ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH1,
                                     ADC_CH_INPUTMODE_SINGLEENDED_gc,
                                     ADC_CH_GAIN_1X_gc);
    ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH2,
                                     ADC_CH_INPUTMODE_DIFF_gc,
                                     ADC_CH_GAIN_1X_gc);
    ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH3,
                                     ADC_CH_INPUTMODE_DIFFWGAIN_gc,
                                     ADC_CH_GAIN_4X_gc);

    ADC_Enable(&ADCA);
	
    ADC_Wait_8MHz(&ADCA);
}
uint16_t * get_readings()
{
	static uint16_t reading[5] = {0};
	static uint16_t stack[5][5] = {0};
	static uint8_t stack_count = 0;
	static uint16_t sum[5] = {0};
	static uint16_t mean[5] = {0};	
	int8_t i;
	
	ADC_Ch_InputMux_Config(&ADCA.CH0, (0 << 3), ADC_CH_MUXNEG_PIN0_gc);
	ADC_Ch_InputMux_Config(&ADCA.CH1, (1 << 3), ADC_CH_MUXNEG_PIN0_gc);
	ADC_Ch_InputMux_Config(&ADCA.CH2, (2 << 3), ADC_CH_MUXNEG_INTGND_MODE4_gc);
	ADC_Ch_InputMux_Config(&ADCA.CH3, (3 << 3), ADC_CH_MUXNEG_INTGND_MODE4_gc);

	ADC_Ch_Conversion_Start(&ADCA.CH0);
	ADC_Ch_Conversion_Start(&ADCA.CH1);
	ADC_Ch_Conversion_Start(&ADCA.CH2);
	ADC_Ch_Conversion_Start(&ADCA.CH3);
	
	while(!ADC_Ch_Conversion_Complete(&ADCA.CH3));

	reading[0] = get_dist(ADC_ResultCh_GetWord_Signed(&ADCA.CH0, ADC_OFFSET));
	reading[1] = get_dist(ADC_ResultCh_GetWord_Signed(&ADCA.CH1, ADC_OFFSET));
	reading[2] = ADC_ResultCh_GetWord_Signed(&ADCA.CH2, 0)/10;
	reading[3] = ADC_ResultCh_GetWord_Signed(&ADCA.CH3, ADC_OFFSET);
	
	ADC_Ch_InputMux_Config(&ADCA.CH0, (4 << 3), ADC_CH_MUXNEG_PIN0_gc);
	ADC_Ch_Conversion_Start(&ADCA.CH0);
	while(!ADC_Ch_Conversion_Complete(&ADCA.CH0));
	reading[4] = get_dist(ADC_ResultCh_GetWord_Signed(&ADCA.CH0, ADC_OFFSET));
		
	for (i=0; i<5 ;i++)
	{
		sum[i] += reading[i] - stack[i][stack_count];
		mean[i] = sum[i]/5;
		stack[i][stack_count] = reading[i];
	}
	stack_count++;
	stack_count%= 5;
	return mean;
}
