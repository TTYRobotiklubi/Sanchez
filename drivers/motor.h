#ifndef MOTOR_H_
#define MOTOR_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "board.h"
#include "drivers/tc_driver.h"
#include "drivers/qdec_driver.h"

#define BIN (1<<7)
#define AIN (1<<6)
void motor_init(void);
void servo_init(void);
void steering(int8_t position, int16_t offset);
int16_t motor_speed(int16_t position);

#endif /* MOTOR_H_ */