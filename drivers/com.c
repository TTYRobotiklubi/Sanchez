#include "com.h"

USART_data_t USART_data;

void radio_init()
{

    COMPORT.DIRSET   = (1<<TXD);

    COMPORT.DIRCLR   = (1<<RXD);
	
    USART_InterruptDriver_Initialize(&USART_data, &RADIO_USART, USART_DREINTLVL_LO_gc);

    USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc, USART_PMODE_DISABLED_gc, false);

    USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);

    USART_Baudrate_Set(&RADIO_USART, (((F_CPU)/(16*57600))) , 0);

    USART_Rx_Enable(USART_data.usart);
    USART_Tx_Enable(USART_data.usart);

    PMIC.CTRL |= PMIC_LOLVLEX_bm;
	
    sei();

}

void radio_puts(char* stringPtr)
{
    while (*stringPtr)
    {
        if(USART_TXBuffer_PutByte(&USART_data, *stringPtr))
        {
            stringPtr++;
        }
    }
}

uint8_t radio_getc()
{
    while (1)
    {
        if(USART_RXBufferData_Available(&USART_data))
        {
            return USART_RXBuffer_GetByte(&USART_data);
        }
    }
}

uint8_t radio_getc_nolock()
{

    if(USART_RXBufferData_Available(&USART_data))
    {
        return USART_RXBuffer_GetByte(&USART_data);
    }
    return 0;
}

ISR( RADIO_USART_RXC_vect )
{
    USART_RXComplete( &USART_data );
}

ISR(RADIO_USART_DRE_vect)
{
    USART_DataRegEmpty( &USART_data );
}