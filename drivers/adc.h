#ifndef ADC_H_
#define ADC_H_

#include <avr/io.h>
#include "drivers/adc_driver.h"

#define ADC_OFFSET 50


void adc_init(void);

uint16_t * get_readings();
#endif /* ADC_H_ */