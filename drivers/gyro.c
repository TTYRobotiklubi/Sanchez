#include "gyro.h"

int32_t gyro_zero = 0;
int32_t gyro = 0;
int8_t gyro_initted = 0;
int8_t gyro_zeroed = 0;
int32_t gyro_angle = 0;

int32_t ang_stack[ANG_STACK_SIZE] = {0};
int8_t ang_cnt = 0;

void gyro_init(void)
{
    TWI_MasterInit(&twiMaster,
                   &QYRO_TWI,
                   TWI_MASTER_INTLVL_HI_gc,
                   TWI_BAUDSETTING);
    PMIC.CTRL |= PMIC_LOLVLEN_bm;
    sei();

    gyro_write_byte(GYRO_ADDRESS,CTRL_REG1,0b11111100);
    gyro_write_byte(GYRO_ADDRESS,CTRL_REG4,0b00110000);
    gyro_write_byte(GYRO_ADDRESS,CTRL_REG5,0b00010000);
	
	TC_SetPeriod(&GYRO_TIMER,32000);
	TC0_ConfigWGM(&GYRO_TIMER,TC_WGMODE_NORMAL_gc);
	TC0_SetOverflowIntLevel(&GYRO_TIMER,TC_OVFINTLVL_MED_gc);
	TC0_ConfigClockSource(&GYRO_TIMER,TC_CLKSEL_DIV1_gc);
	
	gyro_initted = 1;
}

void gyro_write_byte(uint8_t slave, uint8_t address, uint8_t data)
{
    uint8_t data_buf[2];
    data_buf[0] = address;
    data_buf[1] = data;
    TWI_MasterWrite(&twiMaster,slave,data_buf,2);
    while (twiMaster.status != TWIM_STATUS_READY);
}

void gyro_read_gyro()
{
	if (!gyro_zeroed || !gyro_initted) return;
    uint8_t data_buf[1];
    data_buf[0] = STATUS_REG|128;
    if((twiMaster.readData[0] & 0b00000100) && (twiMaster.status == TWIM_STATUS_READY))
    {
        gyro = ((twiMaster.readData[6]<<8)|twiMaster.readData[5]) - gyro_zero;
        TWI_MasterWriteRead(&twiMaster,GYRO_ADDRESS,data_buf,1,7);
    }
    else
    {
	    TWI_MasterWriteRead(&twiMaster,GYRO_ADDRESS,data_buf,1,7);
    }
}
void gyro_zero_gyro()
{
	if(!gyro_initted) return;
	uint8_t data_buf[1];
	data_buf[0] = STATUS_REG|128;
	do
	{
		TWI_MasterWriteRead(&twiMaster,GYRO_ADDRESS,data_buf,1,7);
		while (twiMaster.status != TWIM_STATUS_READY);
	}
	while (!(twiMaster.readData[0] & 0b00000100));

	int16_t i;
	for (i=0;i<1000;i++)
	{
		gyro_zero += ((twiMaster.readData[6]<<8)|twiMaster.readData[5]);
	}
	gyro_zero /= 1000;
	gyro_zeroed = 1;

}
int32_t get_angle()
{
	cli();
	int32_t temp = gyro_angle;
	sei();
	return temp;
}
int32_t get_gyro()
{
	cli();
	int32_t temp = gyro;
	sei();
	return temp;
}
int32_t get_last_good_angle()
{
	cli();
	int32_t temp = ang_stack[(ang_cnt+1)%ANG_STACK_SIZE];
	sei();
	return temp;
}
int32_t get_gyro_zero()
{
	cli();
	int32_t temp = gyro_zero;
	sei();
	return temp;
}
ISR(TWIE_TWIM_vect)
{
    TWI_MasterInterruptHandler(&twiMaster);
}
ISR(GYRO_TIMER_INTERRUPT)
{
	static uint8_t gyro_ticker = 0;
	gyro_ticker ++;
	gyro_ticker %= ANGLE_INTERVAL;
	gyro_read_gyro();
	gyro_angle += (gyro/500);
	gyro_angle %= GYRO_CIRCLE_CAP;
	if (gyro_angle<0)
		gyro_angle+=GYRO_CIRCLE_CAP;	
	if (!gyro_ticker)
	{
		ang_stack[ang_cnt] = gyro_angle;
		ang_cnt++;
		ang_cnt%=ANG_STACK_SIZE;
	}
}