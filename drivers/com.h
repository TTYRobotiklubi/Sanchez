#ifndef COM_H_
#define COM_H_

#include <stdbool.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "board.h"
#include "drivers/usart_driver.h"

#define RADIO_USART USARTE0
#define RADIO_USART_RXC_vect USARTE0_RXC_vect
#define RADIO_USART_DRE_vect USARTE0_DRE_vect

void radio_init();
void radio_puts(char*);
uint8_t radio_getc();
uint8_t radio_getc_nolock();

#endif /* COM_H_ */