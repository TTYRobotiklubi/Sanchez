import serial
import re
import matplotlib.pyplot as plt
import keyboard
import numpy as np
import time
from matplotlib import style

ser = serial.Serial(
    port='COM4',\
    baudrate=9600,\
    parity=serial.PARITY_NONE,\
    stopbits=serial.STOPBITS_ONE,\
    bytesize=serial.EIGHTBITS,\
        timeout=0)
print("connected to: " + ser.portstr)

style.use('fivethirtyeight')

i = 0
seq = []
y = list(range(200))
x = list(range(200))
y1 = list(range(200))
y2 = list(range(200))

plt.ion()
fig = plt.figure()
ax=fig.add_subplot(3,1,1)
ax.set_ylim([0,10000])
ax1=fig.add_subplot(3,1,2)
ax1.set_ylim([-5000,5000])
ax2=fig.add_subplot(3,1,3)
ax2.set_ylim([7000,8400])
filename = str(time.time())
filename = filename + ".csv"
f = open(filename, "a")
f.write(",left,mid,right,current,speed,voltage,gyro,angle,millis")
while True:
    if keyboard.is_pressed('i'):#if key 'q' is pressed
        while(keyboard.is_pressed('i')):
            staller=0
        ser.write(b'\'q')
    if keyboard.is_pressed('g'):#if key 'q' is pressed
        while(keyboard.is_pressed('g')):
            staller=0
        ser.write(b'\'g')
    if keyboard.is_pressed('h'):#if key 'q' is pressed
        while(keyboard.is_pressed('h')):
            staller=0
        ser.write(b'\'h')
    if keyboard.is_pressed('w'):#if key 'q' is pressed
        while(keyboard.is_pressed('w')):
            staller=0
        ser.write(b'\'w')
    if keyboard.is_pressed('s'):#if key 'q' is pressed
        while(keyboard.is_pressed('s')):
            staller=0
        ser.write(b'\'s')
    if keyboard.is_pressed('p'):#if key 'q' is pressed
        while(keyboard.is_pressed('p')):
            staller=0
        ser.write(b'\'p')
    if keyboard.is_pressed('o'):#if key 'q' is pressed
        while(keyboard.is_pressed('o')):
            staller=0
        ser.write(b'\'o')
        
    for c in ser.read():
        seq.append(chr(c)) #convert from ANSII
        joined_seq = ''.join(str(v) for v in seq) #Make a string from array

        if chr(c) == '\n':
            line = joined_seq.split(',')

            f.write(str(line))
            f.write('\n')
            seq = []
            if (len(line) == 11):
                y2.append(int(line[6]))
                y2 = y2[-200:]
                y1.append(int(line[7]))
                y1 = y1[-200:]
                y.append(int(line[8]))
                y = y[-200:]
                i = i+1
                i %= 1000
                if (i%10 == 0):
                    ax2.clear()
                    ax2.plot(x,y2)
                    ax1.clear()
                    ax1.plot(x,y1)
                    ax.clear()
                    ax.plot(x,y)
                    ax.set_ylim([0,10000])
                    ax2.set_ylim([7000,8400])
                    ax1.set_ylim([-5000,5000])
                    fig.canvas.draw()
                    fig.canvas.flush_events()
        break

ser.close()
